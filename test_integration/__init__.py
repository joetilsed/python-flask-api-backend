"""
Integration Tests root
"""

import requests


DOMAIN = "http://localhost:5000"

error_status = "**** Unable to connect to the app ({}) ****".format(DOMAIN)

try:
    test_call = requests.get(url="{}/api/ping".format(DOMAIN))
except requests.exceptions.ConnectionError:
    print(error_status)
    exit(error_status)
else:
    if test_call.status_code != 200:
        print(error_status)
        exit(error_status)

print("Running integration tests on domain: {}".format(DOMAIN))


# That's all folks...
