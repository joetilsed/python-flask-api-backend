"""
Integration Tests for the api heartbeat endpoint
"""

import json
import requests

from unittest import TestCase

from test_integration import DOMAIN


class TestIntegrationAPIPing(TestCase):
    """
    Integration Tests for the api heartbeat endpoint
    """

    def test_ping(self):
        expected_status = 200
        expected_content = {'ping': "Connection Working"}
        resp = requests.get(url="{}/api/ping".format(DOMAIN))
        actual_status = resp.status_code
        actual_content = json.loads(resp.content)
        self.assertEqual(expected_status, actual_status)
        self.assertEqual(expected_content, actual_content)


# That's all folks...
