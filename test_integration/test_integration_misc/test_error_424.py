"""
Integration Tests for a 424 error
"""

import json
import requests

from unittest import TestCase

from test_integration import DOMAIN


class TestIntegration424(TestCase):
    """
    Integration Tests for a 424 error
    """

    def test_424(self):
        expected_status = 424
        expected_content = {
            "ERROR 424":
                "Failed dependency issue. It's not your fault, its not our fault, its THEIR fault! "
                "********THIS WAS A FORCED ERROR AND IS NOT REAL********"
        }
        resp = requests.get(url="{}/api/force_error/503".format(DOMAIN))
        actual_status = resp.status_code
        actual_content = json.loads(resp.content)
        self.assertEqual(expected_status, actual_status)
        self.assertEqual(expected_content, actual_content)


# That's all folks...
