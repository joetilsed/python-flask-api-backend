"""
Integration Tests for a 400 error
"""

import json
import requests

from unittest import TestCase

from test_integration import DOMAIN


class TestIntegration400(TestCase):
    """
    Integration Tests for a 400 error
    """

    def test_400(self):
        expected_status = 400
        expected_content = {
            "ERROR 400":
                "Bad request, check your code is right, required fields entered? "
                "********THIS WAS A FORCED ERROR AND IS NOT REAL********"
        }
        resp = requests.get(url="{}/api/force_error/400".format(DOMAIN))
        actual_status = resp.status_code
        actual_content = json.loads(resp.content)
        self.assertEqual(expected_status, actual_status)
        self.assertEqual(expected_content, actual_content)


# That's all folks...
