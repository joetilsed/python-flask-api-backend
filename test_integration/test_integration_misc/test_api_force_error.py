"""
Integration Tests for forcing an error
"""

import json
import requests

from unittest import TestCase

from test_integration import DOMAIN


class TestIntegrationForceError(TestCase):
    """
    Integration Tests for forcing an error
    """

    def test_valid(self):
        expected_status = 400
        expected_content = {
            "ERROR 400":
                "Bad request, check your code is right, required fields entered? "
                "********THIS WAS A FORCED ERROR AND IS NOT REAL********"
        }
        resp = requests.get(url="{}/api/force_error/400".format(DOMAIN))
        actual_status = resp.status_code
        actual_content = json.loads(resp.content)
        self.assertEqual(expected_status, actual_status)
        self.assertEqual(expected_content, actual_content)

    def test_invalid(self):
        expected_status = 404
        expected_content = {
            "ERROR 404":
                "Not found, we search and searched but nothing there... Sorry. "
                "The requested URL was not found on the server. "
                "If you entered the URL manually please check your spelling and try again."
        }
        resp = requests.get(url="{}/api/force_error/invalid_error_code".format(DOMAIN))
        actual_status = resp.status_code
        actual_content = json.loads(resp.content)
        self.assertEqual(expected_status, actual_status)
        self.assertEqual(expected_content, actual_content)


# That's all folks...
