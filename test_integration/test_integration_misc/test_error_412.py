"""
Integration Tests for a 412 error
"""

import requests

from unittest import TestCase

from test_integration import DOMAIN


class TestIntegration412(TestCase):
    """
    Integration Tests for a 412 error
    """

    def test_412(self):
        expected_status = 302
        expected_content = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\">\n" \
                           "<title>Redirecting...</title>\n" \
                           "<h1>Redirecting...</h1>\n" \
                           "<p>You should be redirected automatically to target URL: " \
                           "<a href=\"********THIS%20WAS%20A%20FORCED%20ERROR%20AND%20IS%20NOT%20REAL********\">" \
                           "********THIS WAS A FORCED ERROR AND IS NOT REAL********</a>.  If not click the link."
        resp = requests.get(url="{}/api/force_error/412".format(DOMAIN))
        actual = resp.history[0]
        actual_status = actual.status_code
        actual_content = actual.content.decode()
        self.assertEqual(expected_status, actual_status)
        self.assertEqual(expected_content, actual_content)


# That's all folks...
