"""
Integration Tests for a 404 error
"""

import json
import requests

from unittest import TestCase

from test_integration import DOMAIN


class TestIntegration404(TestCase):
    """
    Integration Tests for a 404 error
    """

    def test_404(self):
        expected_status = 404
        expected_content = {
            "ERROR 404":
                "Not found, we search and searched but nothing there... Sorry. "
                "********THIS WAS A FORCED ERROR AND IS NOT REAL********"
        }
        resp = requests.get(url="{}/api/force_error/404".format(DOMAIN))
        actual_status = resp.status_code
        actual_content = json.loads(resp.content)
        self.assertEqual(expected_status, actual_status)
        self.assertEqual(expected_content, actual_content)


# That's all folks...
