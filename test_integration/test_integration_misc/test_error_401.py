"""
Integration Tests for a 401 error
"""

import json
import requests

from unittest import TestCase

from test_integration import DOMAIN


class TestIntegration401(TestCase):
    """
    Integration Tests for a 401 error
    """

    def test_401(self):
        expected_status = 401
        expected_content = {
            "ERROR 401":
                "Unauthorized, 'YOU SHALL NOT PASS!' "
                "********THIS WAS A FORCED ERROR AND IS NOT REAL********"
        }
        resp = requests.get(url="{}/api/force_error/401".format(DOMAIN))
        actual_status = resp.status_code
        actual_content = json.loads(resp.content)
        self.assertEqual(expected_status, actual_status)
        self.assertEqual(expected_content, actual_content)


# That's all folks...
