"""
Integration Tests for the index
"""

import json
import requests

from unittest import TestCase

from test_integration import DOMAIN


class TestIntegrationIndex(TestCase):
    """
    Integration Tests for the index
    """

    def test_index(self):
        expected_status = 200
        expected_content = {'index': "Hello World"}
        resp = requests.get(url="{}".format(DOMAIN))
        actual_status = resp.status_code
        actual_content = json.loads(resp.content)
        self.assertEqual(expected_status, actual_status)
        self.assertEqual(expected_content, actual_content)


# That's all folks...
