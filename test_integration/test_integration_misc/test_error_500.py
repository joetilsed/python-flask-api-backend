"""
Integration Tests for a 500 error
"""

import json
import requests

from unittest import TestCase

from test_integration import DOMAIN


class TestIntegration500(TestCase):
    """
    Integration Tests for a 500 error
    """

    def test_500(self):
        expected_status = 500
        expected_content = {
            "ERROR 500":
                "Oops! This is our fault, please contact us (Joe@Tilsed.com) and let us know! "
                "********THIS WAS A FORCED ERROR AND IS NOT REAL********"
        }
        resp = requests.get(url="{}/api/force_error/500".format(DOMAIN))
        actual_status = resp.status_code
        actual_content = json.loads(resp.content)
        self.assertEqual(expected_status, actual_status)
        self.assertEqual(expected_content, actual_content)


# That's all folks...
