"""
Integration Tests for a 405 error
"""

import json
import requests

from unittest import TestCase

from test_integration import DOMAIN


class TestIntegration405(TestCase):
    """
    Integration Tests for a 405 error
    """

    def test_405(self):
        expected_status = 405
        expected_content = {
            "ERROR 405":
                "Method not allowed, you are not allowed to use that call type! "
                "The method is not allowed for the requested URL."
        }
        resp = requests.post(url="{}/api/force_error/405".format(DOMAIN))
        actual_status = resp.status_code
        actual_content = json.loads(resp.content)
        self.assertEqual(expected_status, actual_status)
        self.assertEqual(expected_content, actual_content)


# That's all folks...
