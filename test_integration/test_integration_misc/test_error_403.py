"""
Integration Tests for a 403 error
"""

import json
import requests

from unittest import TestCase

from test_integration import DOMAIN


class TestIntegration403(TestCase):
    """
    Integration Tests for a 403 error
    """

    def test_403(self):
        expected_status = 403
        expected_content = {
            "ERROR 403":
                "Forbidden, do you have the required access to view this? "
                "********THIS WAS A FORCED ERROR AND IS NOT REAL********"
        }
        resp = requests.get(url="{}/api/force_error/403".format(DOMAIN))
        actual_status = resp.status_code
        actual_content = json.loads(resp.content)
        self.assertEqual(expected_status, actual_status)
        self.assertEqual(expected_content, actual_content)


# That's all folks...
