"""
Integration Tests for a 409 error
"""

import json
import requests

from unittest import TestCase

from test_integration import DOMAIN


class TestIntegration409(TestCase):
    """
    Integration Tests for a 409 error
    """

    def test_409(self):
        expected_status = 409
        expected_content = {
            "ERROR 409":
                "Request could not be completed due to a constraint, business reason or an inconsistent state "
                "********THIS WAS A FORCED ERROR AND IS NOT REAL********"
        }
        resp = requests.get(url="{}/api/force_error/409".format(DOMAIN))
        actual_status = resp.status_code
        actual_content = json.loads(resp.content)
        self.assertEqual(expected_status, actual_status)
        self.assertEqual(expected_content, actual_content)


# That's all folks...
