"""
Unit Tests for auth instagram
"""

from unittest import TestCase
from unittest.mock import Mock

from auth.instagram import Instagram


class TestAuthInstagram(TestCase):
    """
    Unit Tests for auth instagram
    """

    def setUp(self):
        self.mock_abort = Mock()
        self.mock_json = Mock()
        self.mock_request = Mock()
        self.mock_requests = Mock()
        self.mock_session = Mock()

        self.mock_instagram_dependencies = {
            'abort': self.mock_abort,
            'json': self.mock_json,
            'request': self.mock_request,
            'requests': self.mock_requests,
            'session': self.mock_session
        }

        self.instagram = Instagram(self.mock_instagram_dependencies)

    def test___init__(self):
        self.assertEqual(self.mock_abort, self.instagram.abort)
        self.assertEqual(self.mock_json, self.instagram.json)
        self.assertEqual(self.mock_request, self.instagram.request)
        self.assertEqual(self.mock_requests, self.instagram.requests)
        self.assertEqual(self.mock_session, self.instagram.session)


# That's all folks...
