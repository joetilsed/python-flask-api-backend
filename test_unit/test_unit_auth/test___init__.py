"""
Unit Tests for auth init
"""

from unittest import TestCase

from auth import instagram_dependencies, instagram


class TestAuthInit(TestCase):
    """
    Unit Tests for auth init
    """

    def test_instagram_dependencies(self):
        expected = sorted(['abort', 'json', 'request', 'requests', 'session'])
        actual = sorted(instagram_dependencies.keys())
        self.assertEqual(expected, actual)

    def test_instagram(self):
        expected = "<class 'auth.instagram.Instagram'>"
        actual = str(type(instagram))
        self.assertEqual(expected, actual)


# That's all folks...
