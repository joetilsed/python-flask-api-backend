"""
Unit Tests for root level config
"""

from unittest import TestCase

from config import CUSTOMER_SUPPORT_EMAIL


class TestConfig(TestCase):
    """
    Unit Tests for root level config
    """

    def test_CUSTOMER_SUPPORT_EMAIL(self):
        expected = "Joe@Tilsed.com"
        actual = CUSTOMER_SUPPORT_EMAIL
        self.assertEqual(expected, actual)


# That's all folks...
