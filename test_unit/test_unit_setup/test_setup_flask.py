"""
Unit Tests for setup setup flask
"""

from unittest import TestCase
from unittest.mock import Mock

from setup.setup_flask import SESSION_LIFETIME_HOURS, APP_SECRET_KEY, SetupFlask


class TestSetupSetupFlask(TestCase):
    """
    Unit Tests for setup setup flask
    """

    def setUp(self):
        self.mock_flask = Mock(return_value='some_app')
        self.setup_flask = SetupFlask(self.mock_flask)

    def test_SESSION_LIFETIME_HOURS(self):
        expected = 8
        actual = SESSION_LIFETIME_HOURS
        self.assertEqual(expected, actual)

    def test_APP_SECRET_KEY(self):
        expected = "Give me all your money freak!!"
        actual = APP_SECRET_KEY
        self.assertEqual(expected, actual)

    def test_flask(self):
        expected = self.mock_flask
        actual = self.setup_flask.flask
        self.assertEqual(expected, actual)

    def test_app(self):
        expected = 'some_app'
        actual = self.setup_flask.app
        self.mock_flask.assert_called_once_with('setup.setup_flask', static_folder="../static", static_url_path="")
        self.assertEqual(expected, actual)


# That's all folks...
