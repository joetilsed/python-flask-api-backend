"""
Unit Tests for setup init
"""

from unittest import TestCase

from unittest.mock import Mock, patch

from setup import API_VERSION, API_PREFIX
from setup import create_app, add_custom_config, setup_blueprints


class TestAuthInit(TestCase):
    """
    Unit Tests for setup init
    """

    def test_API_VERSION(self):
        expected = "v1.0"
        actual = API_VERSION
        self.assertEqual(expected, actual)

    def test_API_PREFIX(self):
        expected = "/api/{}".format(API_VERSION)
        actual = API_PREFIX
        self.assertEqual(expected, actual)

    def test_create_app(self):
        expected = "<class 'flask.app.Flask'>"
        actual = str(type(create_app()))
        self.assertEqual(expected, actual)

    def test_add_custom_config(self):
        expected = {'JSON_SORT_KEYS': False}
        mock_app_object = Mock(config={})
        add_custom_config(mock_app_object)
        actual = mock_app_object.config
        self.assertEqual(expected, actual)

    @patch('misc.misc_controller.register', return_value='mock_misc_controller_register')
    def test_setup_blueprints(self, mock_misc_controller_register):
        mock_app_object = Mock()
        mock_app_object.register_blueprint = Mock()
        setup_blueprints(mock_app_object)
        mock_app_object.register_blueprint.assert_called_once_with(mock_misc_controller_register)


# That's all folks...
