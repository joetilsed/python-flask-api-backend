"""
Unit Tests for root level init
"""

from unittest import TestCase

from src import author, author_email, creation_date


class TestInit(TestCase):
    """
    Unit Tests for root level init
    """

    def test_author(self):
        expected = "Joe Tilsed"
        actual = author
        self.assertEqual(expected, actual)

    def test_author_email(self):
        expected = "Joe@Tilsed.com"
        actual = author_email
        self.assertEqual(expected, actual)

    def test_creation_date(self):
        expected = "20/11/2020"
        actual = creation_date
        self.assertEqual(expected, actual)


# That's all folks...
