"""
Unit Tests for the app module
"""

from unittest import TestCase, mock

from unittest.mock import Mock

import app


class TestApp(TestCase):
    """
    Unit Tests for the app module
    """

    def test_HOST(self):
        expected = "0.0.0.{}".format(0)
        actual = app.HOST
        self.assertEqual(expected, actual)

    def test_PORT(self):
        expected = 5000
        actual = app.PORT
        self.assertEqual(expected, actual)

    def test_DEBUG(self):
        expected = False
        actual = app.DEBUG
        self.assertEqual(expected, actual)

    def test_create_app_object(self):
        expected = "<class 'flask.app.Flask'>"
        actual = str(type(app.create_app_object()))
        self.assertEqual(expected, actual)

    def test_init_(self):
        dummy_app_object = 'dummy_app_object'
        with mock.patch.object(app, "__name__", "__main__"):
            with mock.patch.object(app, 'start') as mock_start:
                app.init(dummy_app_object)
                assert mock_start.call_args[0][0] == dummy_app_object

    def test_start(self):
        mock_app_object = Mock()
        mock_app_object.run = Mock()
        app.start(mock_app_object)
        mock_app_object.run.assert_called_once_with(host=app.HOST, port=app.PORT, debug=app.DEBUG)


# That's all folks...
