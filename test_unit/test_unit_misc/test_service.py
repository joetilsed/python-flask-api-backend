"""
Unit Tests for misc service
"""

from unittest import TestCase
from unittest.mock import Mock

from misc.service import MiscService


class TestMiscService(TestCase):
    """
    Unit Tests for misc service
    """

    def setUp(self):
        self.mock_abort = Mock()
        self.mock_customer_support_email = "Joe@Tilsed.com"

        self.mock_misc_service_dependencies = {
            'abort': self.mock_abort,
            'customer_support_email': self.mock_customer_support_email
        }

        self.misc_service = MiscService(self.mock_misc_service_dependencies)

    def test___init__(self):
        self.assertEqual(self.mock_abort, self.misc_service.abort)
        self.assertEqual(self.mock_customer_support_email, self.misc_service.customer_support_email)

    def test_handle_424_valid(self):
        base_error_message = "Failed dependency issue. It's not your fault, its not our fault, its THEIR fault! "
        expected = base_error_message + 'some_error'
        mock_error = Mock(description='some_error')
        # self.misc_service.global_services.email.error_424 = Mock() TODO
        actual = self.misc_service.handle_424(mock_error)
        # self.misc_service.global_services.email.error_424.assert_called_once_with(expected) TODO
        self.assertEqual(expected, actual)

    def test_handle_424_invalid(self):
        base_error_message = "Failed dependency issue. It's not your fault, its not our fault, its THEIR fault! "
        expected = base_error_message
        dummy_error = 'some_error'
        # self.misc_service.global_services.email.error_424 = Mock() TODO
        actual = self.misc_service.handle_424(dummy_error)
        # self.misc_service.global_services.email.error_424.assert_called_once_with(expected) TODO
        self.assertEqual(expected, actual)

    def test_handle_500_valid(self):
        base_error_message = "Oops! This is our fault, please contact us ({}) and let us know! ".format(
            self.mock_customer_support_email
        )
        expected = base_error_message + 'some_error'
        mock_error = Mock(description='some_error')
        # self.misc_service.global_services.email.error_500 = Mock() TODO
        actual = self.misc_service.handle_500(mock_error)
        # self.misc_service.global_services.email.error_500.assert_called_once_with(expected) TODO
        self.assertEqual(expected, actual)

    def test_handle_500_invalid(self):
        base_error_message = "Oops! This is our fault, please contact us ({}) and let us know! ".format(
            self.mock_customer_support_email
        )
        expected = base_error_message
        dummy_error = 'some_error'
        # self.misc_service.global_services.email.error_500 = Mock() TODO
        actual = self.misc_service.handle_500(dummy_error)
        # self.misc_service.global_services.email.error_500.assert_called_once_with(expected) TODO
        self.assertEqual(expected, actual)

    def test_force_error_valid(self):
        # self.misc_service.auth.validate_admin = Mock() TODO
        dummy_error_code = "400"
        self.misc_service.force_error(dummy_error_code)
        # self.misc_service.auth.validate_admin.assert_called_once_with() TODO
        self.misc_service.abort.assert_called_once_with(400, "********THIS WAS A FORCED ERROR AND IS NOT REAL********")

    def test_force_error_invalid(self):
        # self.misc_service.auth.validate_admin = Mock() TODO
        dummy_error_code = "some_invalid_error_code"
        self.misc_service.force_error(dummy_error_code)
        # self.misc_service.auth.validate_admin.assert_called_once_with() TODO
        self.misc_service.abort.assert_called_once_with(404)


# That's all folks...
