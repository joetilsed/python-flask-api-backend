"""
Unit Tests for misc controller
"""

from unittest import TestCase
from unittest.mock import Mock, call

from misc.controller import MiscController


class TestMiscController(TestCase):
    """
    Unit Tests for misc controller
    """

    def setUp(self):
        self.mock_app = Mock()
        self.mock_blueprint = Mock()
        self.mock_jsonify = Mock(return_value='some_json_output')
        self.mock_redirect = Mock()
        self.mock_misc_service = Mock()

        self.mock_misc_controller_dependencies = {
            'app': self.mock_app,
            'blueprint': self.mock_blueprint,
            'jsonify': self.mock_jsonify,
            'redirect': self.mock_redirect,
            'misc_service': self.mock_misc_service
        }

        self.misc_controller = MiscController(self.mock_misc_controller_dependencies)

    def test___init__(self):
        self.misc_controller.blueprint.assert_called_once_with('Misc', 'misc.controller')
        self.assertEqual(self.mock_app, self.misc_controller.app)
        self.assertEqual(self.mock_blueprint, self.misc_controller.blueprint)
        self.assertEqual(self.mock_jsonify, self.misc_controller.jsonify)
        self.assertEqual(self.mock_redirect, self.misc_controller.redirect)
        self.assertEqual(self.mock_misc_service, self.misc_controller.misc_service)
        self.assertEqual(self.mock_blueprint(), self.misc_controller.register)

        app_errorhandler_calls = [
            call(400), call()(self.misc_controller.error_400),
            call(401), call()(self.misc_controller.error_401),
            call(403), call()(self.misc_controller.error_403),
            call(404), call()(self.misc_controller.error_404),
            call(405), call()(self.misc_controller.error_405),
            call(409), call()(self.misc_controller.error_409),
            call(412), call()(self.misc_controller.error_412),
            call(503), call()(self.misc_controller.error_424),
            call(500), call()(self.misc_controller.error_500)
        ]

        route_calls = [
            call(
                "/api/force_error/<string:error_code>/", methods=['GET'],
                strict_slashes=False, endpoint='api/force_error'
            ), call()(self.misc_controller.force_error),
            call(
                "/", methods=['GET'], strict_slashes=False, endpoint='index'
            ), call()(self.misc_controller.index),
            call(
                "/api/ping/", methods=['GET'], strict_slashes=False, endpoint='api/ping'
            ), call()(self.misc_controller.ping),
        ]

        self.misc_controller.register.app_errorhandler.assert_has_calls(app_errorhandler_calls)
        self.misc_controller.register.route.assert_has_calls(route_calls)

    def test_error_400(self):
        expected = 'some_json_output', 400
        mock_error = Mock(description='some_error')
        actual = self.misc_controller.error_400(mock_error)
        self.misc_controller.jsonify.assert_called_once_with({
            "ERROR 400":
                "Bad request, check your code is right, required fields entered? " + 'some_error'
        })
        self.assertEqual(expected, actual)

    def test_error_401(self):
        expected = 'some_json_output', 401
        mock_error = Mock(description='some_error')
        actual = self.misc_controller.error_401(mock_error)
        self.misc_controller.jsonify.assert_called_once_with({
            "ERROR 401":
                "Unauthorized, 'YOU SHALL NOT PASS!' " + 'some_error'
        })
        self.assertEqual(expected, actual)

    def test_error_403(self):
        expected = 'some_json_output', 403
        mock_error = Mock(description='some_error')
        actual = self.misc_controller.error_403(mock_error)
        self.misc_controller.jsonify.assert_called_once_with({
            "ERROR 403":
                "Forbidden, do you have the required access to view this? " + 'some_error'
        })
        self.assertEqual(expected, actual)

    def test_error_404(self):
        expected = 'some_json_output', 404
        mock_error = Mock(description='some_error')
        actual = self.misc_controller.error_404(mock_error)
        self.misc_controller.jsonify.assert_called_once_with({
            "ERROR 404":
                "Not found, we search and searched but nothing there... Sorry. " + 'some_error'
        })
        self.assertEqual(expected, actual)

    def test_error_405(self):
        expected = 'some_json_output', 405
        mock_error = Mock(description='some_error')
        actual = self.misc_controller.error_405(mock_error)
        self.misc_controller.jsonify.assert_called_once_with({
            "ERROR 405":
                "Method not allowed, you are not allowed to use that call type! " + 'some_error'
        })
        self.assertEqual(expected, actual)

    def test_error_409(self):
        expected = 'some_json_output', 409
        mock_error = Mock(description='some_error')
        actual = self.misc_controller.error_409(mock_error)
        self.misc_controller.jsonify.assert_called_once_with({
            "ERROR 409":
                "Request could not be completed due to a constraint, "
                "business reason or an inconsistent state " + 'some_error'
        })
        self.assertEqual(expected, actual)

    def test_error_412(self):
        expected = 'some_redirect', 302
        mock_error = Mock(description='some_url')
        self.misc_controller.redirect.return_value = 'some_redirect'
        actual = self.misc_controller.error_412(mock_error)
        self.misc_controller.redirect.assert_called_once_with('some_url')
        self.assertEqual(expected, actual)
    
    def test_error_424(self):
        expected = 'some_json_output', 424
        mock_error = Mock(description='some_error')
        self.misc_controller.misc_service.handle_424 = Mock(return_value='some_handle_424_error')
        actual = self.misc_controller.error_424(mock_error)
        self.misc_controller.misc_service.handle_424.assert_called_once_with(mock_error)
        self.misc_controller.jsonify.assert_called_once_with({
            "ERROR 424":
                'some_handle_424_error'
        })
        self.assertEqual(expected, actual)

    def test_error_500(self):
        expected = 'some_json_output', 500
        mock_error = Mock(description='some_error')
        self.misc_controller.misc_service.handle_500 = Mock(return_value='some_handle_500_error')
        actual = self.misc_controller.error_500(mock_error)
        self.misc_controller.misc_service.handle_500.assert_called_once_with(mock_error)
        self.misc_controller.jsonify.assert_called_once_with({
            "ERROR 500":
                'some_handle_500_error'
        })
        self.assertEqual(expected, actual)        
    
    def test_force_error(self):
        dummy_error_code = "some_error_code"
        self.misc_controller.misc_service.force_error = Mock()
        self.misc_controller.force_error(dummy_error_code)
        self.misc_controller.misc_service.force_error.assert_called_once_with(dummy_error_code)

    def test_index(self):
        expected = 'some_json_output', 200
        actual = self.misc_controller.index()
        self.misc_controller.jsonify.assert_called_once_with({'index': "Hello World"})
        self.assertEqual(expected, actual)

    def test_ping(self):
        expected = 'some_json_output', 200
        actual = self.misc_controller.ping()
        self.misc_controller.jsonify.assert_called_once_with({'ping': "Connection Working"})
        self.assertEqual(expected, actual)


# That's all folks...
