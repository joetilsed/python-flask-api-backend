"""
Unit Tests for misc init
"""

from unittest import TestCase

from misc import misc_service_dependencies, misc_service, misc_controller, misc_controller_dependencies


class TestMiscInit(TestCase):
    """
    Unit Tests for misc init
    """

    def test_misc_service_dependencies(self):
        expected = sorted(['abort', 'customer_support_email'])
        actual = sorted(misc_service_dependencies.keys())
        self.assertEqual(expected, actual)

    def test_misc_service(self):
        expected = "<class 'misc.service.MiscService'>"
        actual = str(type(misc_service))
        self.assertEqual(expected, actual)

    def test_misc_controller_dependencies(self):
        expected = sorted(['app', 'blueprint', 'jsonify', 'redirect', 'misc_service'])
        actual = sorted(misc_controller_dependencies.keys())
        self.assertEqual(expected, actual)

    def test_misc_controller(self):
        expected = "<class 'misc.controller.MiscController'>"
        actual = str(type(misc_controller))
        self.assertEqual(expected, actual)


# That's all folks...
