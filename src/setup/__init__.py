"""
Here is where the actual app will be setup
"""

from setup.setup_flask import app

from misc import misc_controller


API_VERSION = "v1.0"
API_PREFIX = "/api/{}".format(API_VERSION)


def create_app():
    add_custom_config(app)
    setup_blueprints(app)
    return app


def add_custom_config(app_object):
    app_object.config['JSON_SORT_KEYS'] = False


def setup_blueprints(app_object):
    app_object.register_blueprint(misc_controller.register)


# That's all folks...
