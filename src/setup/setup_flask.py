"""
Setting up the flask app object
"""

from flask import Flask
from flask_cors import CORS
from datetime import timedelta


SESSION_LIFETIME_HOURS = 8
APP_SECRET_KEY = "Give me all your money freak!!"


class SetupFlask(object):
    """
    Setting up the flask app object
    """

    def __init__(self, flask):
        self.flask = flask
        self.app = self.flask(__name__, static_folder="../static", static_url_path="")


app = SetupFlask(Flask).app
app.secret_key = APP_SECRET_KEY
app.permanent_session_lifetime = timedelta(hours=SESSION_LIFETIME_HOURS)
CORS(app)


# That's all folks...
