"""
Auth > Instagram Class
---
Authenticates users to Instagram
"""


class Instagram(object):
    """
    Auth > Instagram Class
    ---
    Authenticates users to Instagram
    """

    def __init__(self, dependencies):
        self.abort = dependencies['abort']
        self.json = dependencies['json']
        self.request = dependencies['request']
        self.requests = dependencies['requests']
        self.session = dependencies['session']


# That's all folks...
