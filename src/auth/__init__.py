"""
Here is the auth module, this is where anything authentication based is carried out
"""

import json
import requests

from flask import abort, request, session

from auth.instagram import Instagram


instagram_dependencies = {
    'abort': abort,
    'json': json,
    'request': request,
    'requests': requests,
    'session': session
}

instagram = Instagram(instagram_dependencies)


# That's all folks...
