"""
App setup.
Going to be using a Flask app as our API backend
"""

import os

from setup import create_app


HOST = "0.0.0.{}".format(0)
PORT = int(os.getenv('PORT', 5000))
DEBUG = False


def create_app_object():
    app_object = create_app()
    return app_object


def init(app_object):
    if __name__ == '__main__':
        start(app_object)


def start(app_object):
    app_object.run(host=HOST, port=PORT, debug=DEBUG)


app = create_app_object()
init(app)


# That's all folks...
