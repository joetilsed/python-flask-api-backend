"""
Here is the misc module
"""

from flask import Blueprint, abort, jsonify, redirect

from config import CUSTOMER_SUPPORT_EMAIL

from setup.setup_flask import app
from misc.service import MiscService
from misc.controller import MiscController


misc_service_dependencies = {
    'abort': abort,
    'customer_support_email': CUSTOMER_SUPPORT_EMAIL
}
misc_service = MiscService(misc_service_dependencies)

misc_controller_dependencies = {
    'app': app,
    'blueprint': Blueprint,
    'jsonify': jsonify,
    'redirect': redirect,
    'misc_service': misc_service
}
misc_controller = MiscController(misc_controller_dependencies)


# That's all folks...
