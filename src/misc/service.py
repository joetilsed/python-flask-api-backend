"""
The misc service class performs the logic for the misc endpoints and error handling app-wide
"""


class MiscService(object):
    """
    The misc service class performs the logic for the misc endpoints and error handling app-wide
    """

    def __init__(self, dependencies):
        self.abort = dependencies['abort']
        self.customer_support_email = dependencies['customer_support_email']

    def handle_424(self, error):
        output = "Failed dependency issue. It's not your fault, its not our fault, its THEIR fault! "
        try:
            error_description = error.description
        except AttributeError:
            pass
        else:
            output += error_description
        finally:
            # self.global_services.email.error_424(output) TODO
            return output

    def handle_500(self, error):
        output = "Oops! This is our fault, please contact us ({}) and let us know! ".format(self.customer_support_email)
        try:
            error_description = error.description
        except AttributeError:
            pass
        else:
            output += error_description
        finally:
            # self.global_services.email.error_500(output) TODO
            return output

    def force_error(self, error_code):
        # self.auth.validate_admin() TODO
        try:
            self.abort(int(error_code), "********THIS WAS A FORCED ERROR AND IS NOT REAL********")
        except (LookupError, ValueError):
            self.abort(404)


# That's all folks...
