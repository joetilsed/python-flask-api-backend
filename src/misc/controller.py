"""
The misc controller class sets up the routes for all misc endpoint and error handling app-wide
"""


class MiscController(object):
    """
    The misc controller class sets up the routes for all misc endpoint and error handling app-wide
    """

    def __init__(self, dependencies):
        self.app = dependencies['app']
        self.blueprint = dependencies['blueprint']
        self.jsonify = dependencies['jsonify']
        self.redirect = dependencies['redirect']
        self.misc_service = dependencies['misc_service']
        self.register = self.blueprint('Misc', __name__)

        self.register.app_errorhandler(400)(self.error_400)
        self.register.app_errorhandler(401)(self.error_401)
        self.register.app_errorhandler(403)(self.error_403)
        self.register.app_errorhandler(404)(self.error_404)
        self.register.app_errorhandler(405)(self.error_405)
        self.register.app_errorhandler(409)(self.error_409)
        self.register.app_errorhandler(412)(self.error_412)
        self.register.app_errorhandler(503)(self.error_424)
        self.register.app_errorhandler(500)(self.error_500)

        self.register.route(
            "/api/force_error/<string:error_code>/", methods=['GET'], strict_slashes=False, endpoint='api/force_error'
        )(self.force_error)
        self.register.route(
            "/", methods=['GET'], strict_slashes=False, endpoint='index'
        )(self.index)
        self.register.route(
            "/api/ping/", methods=['GET'], strict_slashes=False, endpoint='api/ping'
        )(self.ping)

    def error_400(self, error):
        return self.jsonify({
            "ERROR 400":
                "Bad request, check your code is right, required fields entered? " + error.description
        }), 400

    def error_401(self, error):
        return self.jsonify({
            "ERROR 401":
                "Unauthorized, 'YOU SHALL NOT PASS!' " + error.description
        }), 401

    def error_403(self, error):
        return self.jsonify({
            "ERROR 403":
                "Forbidden, do you have the required access to view this? " + error.description
        }), 403

    def error_404(self, error):
        return self.jsonify({
            "ERROR 404":
                "Not found, we search and searched but nothing there... Sorry. " + error.description
        }), 404

    def error_405(self, error):
        return self.jsonify({
            "ERROR 405":
                "Method not allowed, you are not allowed to use that call type! " + error.description
        }), 405

    def error_409(self, error):
        return self.jsonify({
            "ERROR 409":
                "Request could not be completed due to a constraint, "
                "business reason or an inconsistent state " + error.description
        }), 409

    def error_412(self, error):
        return self.redirect(error.description), 302

    def error_424(self, error):
        return self.jsonify({
            'ERROR 424':
                self.misc_service.handle_424(error)
        }), 424

    def error_500(self, error):
        return self.jsonify({
            "ERROR 500":
                self.misc_service.handle_500(error)
        }), 500

    def force_error(self, error_code):
        self.misc_service.force_error(error_code)

    def index(self):
        return self.jsonify({'index': "Hello World"}), 200

    def ping(self):
        return self.jsonify({'ping': "Connection Working"}), 200


# That's all folks...
